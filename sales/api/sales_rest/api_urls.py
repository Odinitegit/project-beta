from django.urls import path


from .api_views import (
list_salespeople, 
list_customers, 
list_sales, 
manage_salesperson_details, 
get_sale_details, 
manage_customer_details,
list_automobiles, 
get_automobile_details, 
get_salesperson_history,
)

urlpatterns = [
    path("salespeople/<int:pk>/", manage_salesperson_details, name="manage_salesperson_details"),
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("sales/salesperson/<int:salesperson_id>/", get_salesperson_history, name="get_salesperson_history"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", manage_customer_details, name="manage_customer_details"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", get_sale_details, name="get_sale_details"),
    path("automobiles/", list_automobiles, name="list_automobiles"),
    path("automobiles/<str:vin>/", get_automobile_details, name="get_autmobile_details"),

]
