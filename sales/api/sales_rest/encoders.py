from common.json import ModelEncoder
from .models import AutomobileVO,Salesperson, Customer, Sale

class AutomobileVODENC(ModelEncoder):
    model = AutomobileVO
    properties = [ "id",
                   "vin",
                   "sold", 
                   ]
class AutomobileVOLENC(ModelEncoder):
    model = AutomobileVO
    properties = [ "vin", 
                  "sold", 
                    "id",]
class CustomerDENC(ModelEncoder):
    model = Customer
    properties = ["first_name", 
                  "last_name", 
                  "phone_number", 
                  "address" ]
class CustomerLENC(ModelEncoder):
    model = Customer
    properties = ["id",
                  "first_name", 
                  "last_name", 
                  "phone_number", 
                  "address" ]
class SalespersonDENC(ModelEncoder):
    model = Salesperson
    properties = ["id",
                  "first_name", 
                  "last_name", 
                  "employee_id", 
                  "sales"]
class SalespersonLENC(ModelEncoder):
    model = Salesperson
    properties = ["id",
                  "first_name", 
                  "last_name", 
                  "employee_id",]
class SaleLENC(ModelEncoder):
    model = Sale
    properties = ["id",
                  "price", 
                  "salesperson", 
                  "customer", 
                  "automobile" ]
    encoders = {
        "automobile": AutomobileVODENC(),
        "salesperson": SalespersonLENC(),
        "customer": CustomerLENC(),
    }
class SaleDENC(ModelEncoder):
    model = Sale
    properties = [
                  "price", 
                  "salesperson", 
                  "customer", 
                  "automobile" ]
    encoders = {
        "automobile": AutomobileVODENC(),
        "salesperson": SalespersonLENC(),
        "customer": CustomerLENC(),
    }




    









