from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .models import AutomobileVO,Salesperson, Customer, Sale
from .encoders import ( AutomobileVODENC,
                        AutomobileVOLENC,
                        SalespersonLENC,
                        CustomerDENC, 
                        CustomerLENC,
                        SaleLENC,
                        SaleDENC,
                        )
                        

@require_http_methods(["GET"])
def list_automobiles(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse({"automobiles": automobiles}, encoder=AutomobileVOLENC, safe=False)

@require_http_methods(["GET","PUT"])
def get_automobile_details(request, vin):
    if request.method == "GET":
        automobile = AutomobileVO.objects.get(vin=vin)
        return JsonResponse(automobile, encoder=AutomobileVODENC, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        AutomobileVO.objects.filter(vin=vin).update(**content)
        update_auto = AutomobileVO.objects.get(vin=vin)
        return JsonResponse(update_auto, encoder=AutomobileVODENC, safe=False)
    

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse({"salespeople": salespeople}, encoder=SalespersonLENC, safe=False)
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(salesperson, encoder=SalespersonLENC, safe=False)

@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerLENC, safe=False)
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(customer, encoder=CustomerDENC, safe=False)

@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse({"sales": sales}, encoder=SaleLENC, safe=False)
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Not found"}, status=400)

        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Not found"}, status=400)

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Not found"}, status=400)

        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=SaleDENC, safe=False)
    

@require_http_methods(["GET","DELETE"])
def get_sale_details(request,pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(sale, encoder=SaleDENC, safe=False)
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"DELETED": count > 0})

@require_http_methods(["GET"])
def get_salesperson_history(request, salesperson_id):
    try:
        salesperson = Salesperson.objects.get(id=salesperson_id)
    except Salesperson.DoesNotExist:
        return JsonResponse({"error": "Salesperson not found"}, status=400)
    sales = Sale.objects.filter(salesperson=salesperson)
    sales_data = list(sales.values())
    return JsonResponse({'sales': sales_data}, safe=False)

@require_http_methods(["GET","DELETE"])
def manage_salesperson_details(request, pk):
    if  request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(salesperson, encoder=SalespersonLENC, safe=False)
    else:
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"DELETED": count > 0})

@require_http_methods(["GET", "DELETE", "PUT"])
def manage_customer_details(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(customer, encoder=CustomerDENC, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        updated_customer = Customer.objects.get(id=pk)
        return JsonResponse(updated_customer, encoder=CustomerDENC, safe=False)
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"DELETED": count > 0})




        
    