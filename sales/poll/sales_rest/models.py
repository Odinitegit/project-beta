from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id =models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("manage_salesperson_details", kwargs={"pk": self.id})

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address =models.CharField(max_length=200)
    phone_number=models.IntegerField()

    def __str__(self):
        return self.first_name

class Sale(models.Model):
    automobile=models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    salesperson=models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=10)

    
    def get_api_url(self):
     return reverse("get_sale_details", kwargs={"pk": self.id})
  





