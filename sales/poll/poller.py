import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something


from sales_rest.models import AutomobileVO


def get_automobiles():
    api_response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles")
    auto_content = json.loads(api_response.content)
    for auto_info in auto_content["autos"]:
        AutomobileVO.objects.update_or_create(
                vin=auto_info["vin"],
                import_href=auto_info["href"],
                defaults={"sold": auto_info["sold"],}
        )


def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
        except Exception as err:
            print(err, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)

if __name__ == "__main__":
    poll()
