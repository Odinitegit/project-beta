import { useEffect, useState } from 'react';

function ListCustomers() {
    const [deleted, setDeleted] = useState(false)
    const [customers, setCustomers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
    
        if (response.ok) {
        const data = await response.json();
            setCustomers(data.customers)
        }
    }

    const deleteCustomer = async (pk) => {
    
        const url = `http://localhost:8090/api/customers/${pk}/`
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
      };
      const response = await fetch(url, fetchConfig);
      
      if (response.ok) {
        getData();
        setDeleted(true);
      }
    };

    useEffect(() => {
        getData()
    }, [deleted])

    return (
        <table className="table table-striped">
            <thead>
                <tr> 
                    <th>Customer Name</th> 
                    <th>Address</th> 
                    <th>Phone Number</th> 
                </tr>
            </thead>
            <tbody>
                { 
                  customers.map((customer) => (
                        <tr key={customer.id}>
                            <td>{customer.first_name + ' ' + customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                            <td>
                            <button className = "btn btn-danger" onClick={() => deleteCustomer(customer.id)}>
                                Delete
                            </button> 
                            </td>
                        </tr>
                    
                  )) 
                }
            </tbody>
        </table>
    );
}

export default ListCustomers;




