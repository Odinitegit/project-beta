import React, { useState } from 'react';

function CreateTechnician() {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    employee_id: '',
  });

  const [createMessage, setCreateMessage] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        setCreateMessage('Technician successfully created!');
      }
    } catch (error) {
      console.error('Error creating technician:', error);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Technician</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <input
                type="text"
                className="form-control"
                name="first_name"
                placeholder="First Name"
                value={formData.first_name}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              />
            </div>
            <div className="mb-3">
              <input
                type="text"
                className="form-control"
                name="last_name"
                placeholder="Last Name"
                value={formData.last_name}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              />
            </div>
            <div className="mb-3">
              <input
                type="text"
                className="form-control"
                name="employee_id"
                placeholder="Employee ID"
                value={formData.employee_id}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              />
            </div>

            <div>
              <button type="submit" className="btn btn-primary">
                Create Technician
              </button>
            </div>
          </form>

          {createMessage && (
            <div style={{ color: createMessage.includes('successfully') ? 'green' : 'red', marginTop: '10px' }}>
              <p>{createMessage}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default CreateTechnician;
