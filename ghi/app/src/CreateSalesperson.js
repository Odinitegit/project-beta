import React, {useState} from 'react';

function CreateSalesperson(){

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    });

    const [hasCreatedNewSalesperson, setCreatedNewSalesperson] = useState(false)


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespeople/`



        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });


            setCreatedNewSalesperson(true);
        }

    };



    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      };

      const formClasses = (!hasCreatedNewSalesperson) ? '' : 'd-none';
      const messageClasses = (!hasCreatedNewSalesperson) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

      return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form className={formClasses} onSubmit={handleSubmit} id="create-new-salesperson-form">
                  <h1 className="card-title">Add a Salesperson</h1>
                        <div className="col">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="col">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="employee_id" type="text" id="employee_id" name="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee ID</label>
                            </div>
                        </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Create</button>
                </form>
                    <div className={messageClasses} id="success-message">
                    New Salesperson Created!
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }
    
          export default CreateSalesperson;






