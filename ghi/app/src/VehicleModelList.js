import React, { useState, useEffect } from 'react';

const VehicleModelList = () => {
  const [models, setVehicleModels] = useState([]);

  const fetchVehicleModels = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/models/');

      if (response.ok) {
        const data = await response.json();
        setVehicleModels(data.models);
      }
    } catch (error) {
      console.error('Error fetching vehicle models:', error.message);
    }
  };

  useEffect(() => {
    fetchVehicleModels();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map((model, index) => (
          <tr key={model.id} style={{ background: index % 2 === 0 ? '#f8f9fa' : 'white' }}>
            <td>{model.name}</td>
            <td>{model.manufacturer.name}</td>
            <td>
              <img
                src={model.picture_url}
                alt="model.pic"
                style={{ width: '100%', height: 'auto', maxWidth: '150px', maxHeight: '150px' }}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default VehicleModelList;
