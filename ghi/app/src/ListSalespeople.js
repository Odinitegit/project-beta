import { useEffect, useState } from 'react';

function ListSalespeople() {
    const [deleted, setDeleted] = useState(false)
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
    
        if (response.ok) {
        const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    const deleteSalesperson = async (pk) => {
    
        const url = `http://localhost:8090/api/salespeople/${pk}/`
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
      };
      const response = await fetch(url, fetchConfig);
      
      if (response.ok) {
        getData();
        setDeleted(true);
      }
    };

    useEffect(() => {
        getData()
    }, [deleted])

    return (
        <table className="table table-striped">
            <thead>
                <tr> 
                    <th>Employee ID</th> 
                    <th>First Name</th> 
                    <th>Last Name</th> 
                </tr>
            </thead>
            <tbody>
                { 
                  salespeople.map((salesperson) => (
                        <tr key={salesperson.id}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>
                            <button className = "btn btn-danger" onClick={() => deleteSalesperson(salesperson.id)}>
                                Delete
                            </button> 
                            </td>
                        </tr>
                    
                  )) 
                }
            </tbody>
        </table>
    );
}

export default ListSalespeople;


                            
                           


