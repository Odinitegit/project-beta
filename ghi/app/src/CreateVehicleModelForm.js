import React, { useState, useEffect } from 'react';

function CreateVehicleModel() {
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  const [manufacturers, setManufacturers] = useState([]);
  const [createMessage, setCreateMessage] = useState('');
  const [createSuccessful, setCreateSuccessful] = useState(false);

  const fetchManufacturers = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const manufacturerData = await response.json();
        setManufacturers(manufacturerData.manufacturers);
      }
    } catch (error) {
      console.error('Error fetching manufacturers:', error);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const handleInputChange = (event, setterFunction) => {
    const value = event.target.value;
    setterFunction(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name: formData.name,
      picture_url: formData.picture_url,
      manufacturer_id: formData.manufacturer_id,
    };

    const modelsUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(modelsUrl, fetchConfig);
      if (response.ok) {
        setCreateSuccessful(true);
        setFormData({
          name: '',
          picture_url: '',
          manufacturer_id: '',
        });
        setCreateMessage('Vehicle model successfully created!');
      } else {
        setCreateMessage('Error creating a vehicle model. Please check your input.');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Vehicle Model</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <input
                type="text"
                className="form-control"
                name="name"
                placeholder="Model Name"
                value={formData.name}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              />
            </div>
            <div className="mb-3">
              <input
                type="text"
                className="form-control"
                name="picture_url"
                placeholder="Picture URL"
                value={formData.picture_url}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              />
            </div>
            <div className="mb-3">
              <select
                className="form-control"
                name="manufacturer_id"
                value={formData.manufacturer_id}
                onChange={(e) => handleInputChange(e, setFormData)}
                required
              >
                <option value="">Select a Manufacturer</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
            </div>

            <div>
              <button type="submit" className="btn btn-primary">
                Create Vehicle Model
              </button>
            </div>
          </form>

          {createMessage && (
            <div style={{ color: createMessage.includes('successfully') ? 'green' : 'red', marginTop: '10px' }}>
              <p>{createMessage}</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default CreateVehicleModel;
