import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import VehicleModelList from './VehicleModelList';
import CreateVehicleModel from './CreateVehicleModelForm';
import TechnicianList from './TechnicianList';
import CreateTechnician from './CreateTechnicianForm';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import CreateManufacturer from './CreateManufacturer';
import AutomobilesList from './AutomobilesList';
import CreateAutomobile from './CreateAutomobile';
import ListCustomers from './ListCustomers';
import CreateCustomer from './CreateCustomer';
import ListSalespeople from './ListSalespeople';
import CreateSalesperson from './CreateSalesperson';
import ListSales from './ListAllSales';
import SalesPersonHistory from './SalesPersonHistory';
import RecordNewSale from './RecordNewSale';
import CreateServiceAppointment from './CreateServiceAppointment';
import ListAppointment from './ListAppointments';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/new" element={<CreateManufacturer />} />
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="/appointments" element={<ListAppointment />} />
          <Route path="/appointment/new" element={<CreateServiceAppointment />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<CreateAutomobile />} />
          <Route path="/customers" element={<ListCustomers />} />
          <Route path="/customers/new" element={<CreateCustomer />} />
          <Route path="/salespeople" element={<ListSalespeople />} />
          <Route path="/salespeople/new" element={<CreateSalesperson />} />
          <Route path="/sales" element={<ListSales />} />
          <Route path="/sales/new" element={<RecordNewSale />} />
          <Route path="/sales/history" element={<SalesPersonHistory />} />
          <Route path="/vehicle-models" element={<VehicleModelList />} />
          <Route path="/create-vehicle-model" element={<CreateVehicleModel />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/new" element={<CreateTechnician />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
