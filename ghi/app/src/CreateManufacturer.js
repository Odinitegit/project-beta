import { useState } from 'react';

function CreateManufacturer(){
    const [formData, setFormData] = useState({
        name: ""
    });

    const [createdNewManufacturer, setCreatedNewManufacturer] = useState(false)

      const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/'
    
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
              name: ""
            });
            

            
            setCreatedNewManufacturer(true);
        }

    };
                



    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      };

      const formClasses = (!createdNewManufacturer) ? '' : 'd-none';
      const messageClasses = (!createdNewManufacturer) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

      return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form className={formClasses} onSubmit={handleSubmit} id="create-new-manufacturer-form">
                  <h1 className="card-title">Create a Manufacturer</h1>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                        <label htmlFor="Manufacturer">Manufacturer</label>
                      </div>
                    <div className="mb-3">
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">Submit New Manufacturer</button>
                </form>

                <div className={messageClasses} id="success-message">
                  New Manufacturer Created!
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }
        
                  






      export default CreateManufacturer;


      