import React, { useEffect, useState } from 'react';

function ListAppointment() {
    const [appointments, setAppointments] = useState([]);
    const [vin, setVin] = useState("");
    const [vipStatus, setVIPStatus] = useState({});

    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments || []);
        }
    }

    const checkVIPStatus = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                const vipStatus = {};
                data.autos.forEach(item => {
                    vipStatus[item.vin] = true;
                });
                setVIPStatus(vipStatus);
            }
        } catch (error) {
            console.error("Error checking VIP status:", error);
        }
    };

    const cancelAppointment = async (id) => {
        const fetchConfig = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchConfig);
        if (response.ok) {
            setAppointments((prevAppointments) =>
                prevAppointments.filter((appointment) => appointment.id !== id)
            );
        }
    }

    const finishAppointment = async (id) => {
        try {
            const fetchConfig = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchConfig);
            if (response.ok) {
                setAppointments((prevAppointments) =>
                    prevAppointments.filter((appointment) => appointment.id !== id)
                );
            }
        } catch (error) {
            console.error("Error finishing appointment", error);
        }
    }

    useEffect(() => {
        getAppointments();
        checkVIPStatus();
    }, []);

    return (
        <div>
            <h1>List of Appointments</h1>
            <table className="table">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Is VIP?</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter((appointment) => appointment.status !== 'canceled' && appointment.status !== 'finished')
                        .map((appointment) => (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{vipStatus[appointment.vin] ? 'no' : 'yes'}</td>
                                <td>
                                    <button type="button" id={appointment.id} onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">
                                        Cancel
                                    </button>
                                    <button type="button" id={appointment.id} onClick={() => finishAppointment(appointment.id)} className="btn btn-success">
                                        Finished
                                    </button>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>
        </div>
    );
}

export default ListAppointment;
