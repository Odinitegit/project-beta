import { useState, useEffect } from 'react';

function CreateAutomobile(){
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        color: "",
        year: "",
        vin: "",
        model_id: ""
    });

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
  
        if (response.ok) {
          const data = await response.json();
          setModels(data.models);
        }
      }
  
      useEffect(() => {
        getData();
      }, []);
    
    
    
    const [createdNewAutomobile, setCreatedNewAutomobile] = useState(false)

      const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/'
    
        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
              color: "",
              year: "",
              vin: "",
              model_id: ""
            });
            
              
            getData();
            setCreatedNewAutomobile(true);
        }

    };
                



    const handleFormChange = (e) => {
      
        const value = e.target.value;
        const inputName = e.target.name;

        
        
        setFormData({
          ...formData,
          [inputName]: value
        });
      };

      const formClasses = (!createdNewAutomobile) ? '' : 'd-none';
      const messageClasses = (!createdNewAutomobile) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

      return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form className={formClasses} onSubmit={handleSubmit} id="create-new-automobile-form">
                  <h1 className="card-title">Add an automobile to inventory.</h1>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="color" type="text" id="color" name="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                      </div>
                    <div className="col">
                     <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="year" type="number" id="style_name" name="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} required placeholder="vin" type="text" id="vin" name="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                    </div>
                    <div className="mb-3">
                <select onChange={handleFormChange} required name="model_id" id="model_id" className="form-select" value = {formData.model}>
                 <option value="">Choose a model...</option>
                {models.map(model => {
                  return (
                    <option key={model.id} value={model.id}>{model.manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
                  </div>
                  <button className="btn btn-lg btn-primary">Create</button>
                </form>
                <div className={messageClasses} id="success-message">
                  New Automobile Created!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }

    export default CreateAutomobile;




                     



        
                  








      