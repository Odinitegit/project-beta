import React, {useState} from 'react';

function CreateNewSale(){

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });

    const [hasCreatedNewCustomer, sethasCreatedNewCustomer] = useState(false)


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/customers/`



        const fetchConfig = {
            method:"post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
            first_name: '',
            last_name: '',
            address: '',
            phone_number: '',
            });


            sethasCreatedNewCustomer(true);
        }

    };



    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
      };

      const formClasses = (!hasCreatedNewCustomer) ? '' : 'd-none';
      const messageClasses = (!hasCreatedNewCustomer) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

      return (
        <div className="my-5">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-body">

                <form className={formClasses} onSubmit={handleSubmit} id="create-new-customer-form">
                  <h1 className="card-title">Add a Customer</h1>
                        <div className="col">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="col">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="address" type="text" id="address" name="address" className="form-control" />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} required placeholder="phone_number" type="number" id="phone_number" name="phone_number" className="form-control" />
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                        </div>
                    </div>
                  <button className="btn btn-lg btn-primary">Create</button>
                </form>
                    <div className={messageClasses} id="success-message">
                    New Customer Created!
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
    }
    
          export default CreateNewSale;






