import { useEffect, useState } from 'react';

function ManufacturerList(props){
    const[manufacturers,setManufacturers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
    
        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers)
          
        }
        
      }

      useEffect(()=>{
        getData()
      }, [])


      return (

        <table className="table table-striped">
        
            <thead>
                  <tr> 
                     <th>Names of Manufacturers</th> 
                     
            
                  </tr>
              </thead>
        
           <tbody>
           {manufacturers.map((manufacturers) => {
                  return (
                 
                    <tr key={manufacturers.id} >
                           <td>{ manufacturers.name }</td>
                         </tr>
                      
                       );
                     })}
                   </tbody>
             
                   </table>
               );
             }
             
             export default ManufacturerList;
