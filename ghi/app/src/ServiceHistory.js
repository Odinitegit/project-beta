import { useEffect, useState } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [searchVIN, setSearchVIN] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const [vipStatus, setVIPStatus] = useState({});
  const [searchNotFound, setSearchNotFound] = useState(false);

  const getAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments');
      if (response.ok) {
        const data = await response.json();
        const allAppointments = data.appointments;

        setAppointments(allAppointments);
        filterAppointments(allAppointments);
      }
    } catch (error) {
      console.error("Error getting appointments", error);
    }
  }

  const filterAppointments = (allAppointments) => {
    const filtered = allAppointments.filter((appointment) =>
      (appointment.status === 'canceled' || appointment.status === 'finished')
    );

    if (searchVIN) {
      const vinFiltered = allAppointments.filter((appointment) =>
        (appointment.status === 'canceled' || appointment.status === 'finished') &&
        appointment.vin.includes(searchVIN)
      );

      if (vinFiltered.length === 0) {
        setSearchNotFound(true);
        setFilteredAppointments([]);
      } else {
        setSearchNotFound(false);
        setFilteredAppointments(vinFiltered);
      }
    } else {
      setSearchNotFound(false);
      setFilteredAppointments(filtered);
    }
  }

  const getVIPStatus = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
        const data = await response.json();
        const vipStatus = {};
        data.autos.forEach(item => {
          vipStatus[item.vin] = true;
        });
        setVIPStatus(vipStatus);
      }
    } catch (error) {
      console.error("Error checking VIP status:", error);
    }
  };

  const handleSearch = () => {
    filterAppointments(appointments);
  };

  const clearSearch = () => {
    setSearchVIN('');
    setSearchNotFound(false);
    setFilteredAppointments(appointments);
  }

  useEffect(() => {
    getAppointments();
    getVIPStatus();
  }, []);

  return (
    <div>
      <h1>Service History</h1>
      <div>
        <label htmlFor="vinSearch">Search by VIN:</label>
        <input
          type="text"
          id="vinSearch"
          value={searchVIN}
          onChange={(e) => setSearchVIN(e.target.value)}
        />
        <button onClick={handleSearch}>Search</button>
        <button onClick={clearSearch}>Clear Search</button>
      </div>
      {searchNotFound && (
        <p style={{ color: 'red' }}>No appointment with that VIN number found.</p>
      )}
      <table className="table">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date & Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{vipStatus[appointment.vin] ? 'yes' : 'no'}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
