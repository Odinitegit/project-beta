import { useEffect, useState } from 'react';

function ListSales() {
    const [deleted, setDeleted] = useState(false)
    const [sales, setSales] = useState([])
    

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
    
        if (response.ok) {
        const data = await response.json();
            setSales(data.sales)
        }
    }

    const deleteSales = async (pk) => {
    
        const url = `http://localhost:8090/api/sales/${pk}/`
        const fetchConfig = {
            method: "DELETE",
            headers: { "Content-Type": "application/json" },
      };
      const response = await fetch(url, fetchConfig);
      
      if (response.ok) {
        getData();
        setDeleted(true);
      }
    };

    useEffect(() => {
        getData()
    }, [deleted])

    return (
        <table className="table table-striped">
            <thead>
                <tr> 
                    <th>Salesperson Employee ID</th> 
                    <th>Salesperson Name</th> 
                    <th>Customer</th> 
                    <th>VIN</th> 
                    <th>Price</th> 
                </tr>
            </thead>
            <tbody>
                { 
                  sales.map((sale) => (
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name + ' ' + sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name + ' ' + sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{"$" + sale.price}</td>
                            <td>
                            <button className = "btn btn-danger" onClick={() => deleteSales(sale.id)}>
                                Delete
                            </button> 
                            </td>
                        </tr>
                    
                  )) 
                }
            </tbody>
        </table>
    );
}

export default ListSales;


                            
                          
                           


