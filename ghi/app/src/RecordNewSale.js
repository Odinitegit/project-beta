import React, {useState,useEffect} from 'react';
function RecordNewSale(){
    const [automobiles, setAutomobile] = useState([]);
    const [salespeople, setSalesperson] = useState([]);
    const [customers, setCustomer] = useState([])
    const [hasCreatedNewSale, sethasCreatedNewSale] = useState(false)

    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
       
    });
    const getAutomobile = async () => {
        const response = await fetch('http://localhost:8090/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.automobiles); 
            
        }
    };
    const getSalespeople = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople); 
        }
    };

    const getCustomers = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers); 
        }
    };
    
    const handleSubmit = async (event) => {
            event.preventDefault();
        
            const url = `http://localhost:8090/api/sales/`
        
            const fetchConfig = {
                method:"post",
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                },
            };
            
            
            const response = await fetch(url, fetchConfig);
            
            if (response.ok) {
              setFormData({
                price:'',
                automobile:'',
                salesperson:'',
                customer:'',
              });
            }
          

            
             if(response.ok){
               
                 const url = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
         
                 const fetchConfig = {
                     method:"put",
                     body: JSON.stringify({'sold':true }),
                     headers: {
                         'Content-Type': 'application/json'
                     },
                 };
                 
                 
                 const responseUpdateVehicle = await fetch(url, fetchConfig);
         
                
                 if (!responseUpdateVehicle.ok) {
                     throw new Error('Failed to update');  
                 }
 
             
                 
                 sethasCreatedNewSale(true);
                 getAutomobile();
             }
         };
         
         useEffect(() => {
             getSalespeople()
             getCustomers()
             getAutomobile();
         }, []);
       
     const handleFormChange = (event) => {
             const value = event.target.value;
             const inputName = event.target.name;  
             
             setFormData({
               ...formData,
               [inputName]: value
               
             });
           };
     
     const formClasses = (!hasCreatedNewSale) ? '' : 'd-none';
     const messageClasses = (!hasCreatedNewSale) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
     
           return (
               <div className="my-5">
                 <div className="row">
                   <div className="col">
                     <div className="card shadow">
                       <div className="card-body">
             
                         <form className={formClasses} onSubmit={handleSubmit} id="create-new-sale-form">
                           <h1 className="card-title">Record a new sale</h1>
             
                           <div className="form-group">
                           <label htmlFor="automobile">Automobile VIN</label>
                           <select name='automobile' id='automobile' className='form-control' value={formData.automobile} onChange={handleFormChange}>
                                 <option value="">Select VIN</option>
                                 {automobiles.map(automobile => {
                                 if(automobile.sold === false){  
                                   return(<option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                    
                                  )};
                               })};
                         </select>
                           </div>
             
                           <div className="form-group">
                             <label htmlFor="salesperson">Salesperson</label>
                             <select name='salesperson' id='salesperson' className='form-control' value={formData.salesperson} onChange={handleFormChange}>
                               <option>Select Salesperson</option>
                               {salespeople.map(salesperson => 
                               <option key ={salesperson.id} value={salesperson.id}>{salesperson.first_name + " " + salesperson.last_name}</option>)};
                             </select>
                           </div>
             
                           <div className="form-group">
                             <label htmlFor="customer">Customer</label>
                             <select name='customer' id='customer' className='form-control' value={formData.customer} onChange={handleFormChange}>
                                 <option value="">Choose a customer...</option>
                                 {customers.map(customer => 
                                 <option key={customer.id} value={customer.id}>{customer.first_name + " " + customer.last_name}</option>)};
                             </select>
                           </div>
                           
                           <div className="form-group">
                             <label htmlFor="price">Price</label>
                             <input type='number' id='price' name='price' className='form-control' placeholder='0' value={formData.price} onChange={handleFormChange}/>
                           </div>
                           <button className="btn btn-lg btn-primary mt-3">Create</button>
                         </form>
                         <div className={messageClasses} id="success-message">
                           New Sale Created!
                         </div>
                       
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             );
             }
             
             export default RecordNewSale;
                                 
 
 
   
               
        
     
 
 
 
 
 
                   
 
     
 
 
 
 
 


             
