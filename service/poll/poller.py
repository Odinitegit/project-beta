import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something


from service.models import Appointment


def get_appointments():
    api_response = requests.get("http://project-beta-service-api-1:8000/api/appointments")
    appointments_data = api_response.json()
    for appointment_info in appointments_data["appointments"]:
        Appointment.objects.update_or_create(
            id=appointment_info["id"],
            defaults={
                "date_time": appointment_info["date_time"],
                "reason": appointment_info["reason"],
                "status": appointment_info["status"],
                "vin": appointment_info["vin"],
                "customer": appointment_info["customer"],
                "technician": appointment_info["technician"],
            }
        )


def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            get_appointments()
        except Exception as err:
            print(err, file=sys.stderr)

        if not repeat:
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
