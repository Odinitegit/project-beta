from django.urls import path
from .views import (
    api_list_technicians,
    api_show_technician,
    api_list_appointments,
    api_create_appointment,
    api_list_automobiles,
    api_list_customers,
    api_service_history,
    api_finish_appointment,
    api_cancel_appointment,
)

urlpatterns = [
    path('technicians/', api_list_technicians, name='api-list-technicians'),
    path('technicians/<int:employee_id>/', api_show_technician, name='api-show-technician'),
    path('technicians/new/', api_list_technicians, name='api-list-technician'),
    path('appointments/', api_list_appointments, name='api-list-appointments'),
    path('appointments/new/', api_create_appointment, name='api-create-appointment'),
    path('automobiles/', api_list_automobiles, name='api-list-automobiles'),
    path('customers/', api_list_customers, name='api-list-customers'),
    path('service-history/', api_service_history, name='api-service-history'),
    path('appointments/<int:appointment_id>/finish/', api_finish_appointment, name='api-finish-appointment'),
    path('appointments/<int:appointment_id>/cancel/', api_cancel_appointment, name='api-cancel-appointment'),
]
