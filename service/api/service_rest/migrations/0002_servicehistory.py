# Generated by Django 4.0.3 on 2023-12-29 05:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceHistory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('VIN', models.CharField(max_length=17, unique=True)),
                ('customer', models.CharField(max_length=100)),
                ('date_time', models.DateTimeField()),
                ('reason', models.CharField(max_length=200)),
                ('status', models.CharField(max_length=200)),
                ('technician', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='service_rest.technician')),
            ],
        ),
    ]
