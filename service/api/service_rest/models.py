from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    technician_id = models.IntegerField(default=0, unique=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    id = models.AutoField(primary_key=True)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})


class ServiceHistory(models.Model):
    VIN = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)

    def __st__(self):
        return self.VIN
