from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO, ServiceHistory
import json
from common.json import ModelEncoder
from django.utils.dateparse import parse_datetime


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "technician_id"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "technician_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "reason", "status", "vin", "customer", "technician", "id"]

    def default(self, obj):
        if isinstance(obj, Technician):
            return TechnicianListEncoder().default(obj)
        return super().default(obj)


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time", "reason", "status", "vin", "customer", "technician", "id"]


class ServiceHistoryEncoder(ModelEncoder):
    model = ServiceHistory
    properties = ["VIN", "Customer", "Date & Time", "Technician", "Reason", "Status", "id"]

    def default(self, obj):
        if isinstance(obj, ServiceHistory):
            return super().default(obj)
        return super().default(obj)


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"created": True, "technician": technician},
            encoder=TechnicianListEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_show_technician(request, technician_id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(technician_id=technician_id)
            technician.delete()
            return JsonResponse({"deleted": True})
        except Technician.DoesNotExist:
            return JsonResponse({"error": f"Technician with employee_id {technician_id} not found"}, status=404)


@require_http_methods(["GET"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["POST"])
def api_create_appointment(request):
    if request.method == "POST":
        content = json.loads(request.body)

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def check_vip_status(vin):
    try:
        automobile = AutomobileVO.objects.get(vin=vin)
        return not automobile.sold
    except AutomobileVO.DoesNotExist:
        return False


@require_http_methods(["GET"])
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        data = {'automobiles': list(automobiles.values())}
        return JsonResponse(data)


@require_http_methods(["GET"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Appointment.objects.values('customer').distinct()
        data = {'customers': list(customers)}
        return JsonResponse(data)


@require_http_methods(["GET"])
def api_service_history(request):
    if request.method == "GET":
        vin = request.GET.get("vin", "")
        appointments = ServiceHistory.objects.filter(vin=vin)
        data = ServiceHistoryEncoder(appointments, many=True).data
        return JsonResponse(data, encoder=ServiceHistoryEncoder, safe=False)


@require_http_methods(["PUT"])
def api_finish_appointment(request, appointment_id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=appointment_id)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse({"message": "Appointment is finished"})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, appointment_id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=appointment_id)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse({"message": "Appointment is canceled"})
