# CarCar

 Team:

 * Aaron Duque - Sales
 * Will Segovia - Service

## Design

![My Project Diagram/Design](./My%20Project%20Diagram2.png)

## Service Microservice
Explain your models and integration with the inventory microservice, here.

Models
Technician Model
  Fields: first_name, last_name, technician_id

Appointment Model
  Fields: date_time, reason, status, vin, customer, technician, id

AutomobileVO Model
  Fields: import_href, vin, sold

ServiceHistory Model
  Fields: VIN, Customer, Date & Time, Technician, Reason, Status, id

Endpoints
Technicians
List Technicians
  Method: GET
  URL: http://localhost:XXXX/api/technicians/

Create Technician
  Method: POST
  URL: http://localhost:XXXX/api/technicians/
  Request Body Example:
  {
    "first_name": "John",
    "last_name": "Doe",
    "technician_id": "JD123"
  }

Get Technician Details
  Method: GET
  URL: http://localhost:XXXX/api/technicians/{technician_id}/

Delete Technician
  Method: DELETE
  URL: http://localhost:XXXX/api/technicians/{technician_id}/

Appointments
List Appointments
  Method: GET
  URL: http://localhost:XXXX/api/appointments/

Create Appointment
  Method: POST
  URL: http://localhost:XXXX/api/appointments/
  Request Body Example:
  {
    "date_time": "2023-01-01T12:00:00Z",
    "reason": "Service Request",
    "status": "Scheduled",
    "vin": "XYZ123",
    "customer": "John Doe",
    "technician": "JD123"
  }

Get Appointment Details
  Method: GET
  URL: http://localhost:XXXX/api/appointments/{appointment_id}/

Finish Appointment
  Method: PUT
  URL: http://localhost:XXXX/api/appointments/{appointment_id}/finish/

Cancel Appointment
  Method: PUT
  URL: http://localhost:XXXX/api/appointments/{appointment_id}/cancel/

Automobiles
List Automobiles
  Method: GET
  URL: http://localhost:XXXX/api/automobiles/

Create Automobile
  Method: POST
  URL: http://localhost:XXXX/api/automobiles/
  Request Body Example:
  {
    "color": "Blue",
    "year": 2022,
    "vin": "XYZ123",
    "model": "Model X"
  }

Get Automobile Details
  Method: GET
  URL: http://localhost:XXXX/api/automobiles/{vin}/

Update Automobile
  Method: PUT
  URL: http://localhost:XXXX/api/automobiles/{vin}/
  Request Body Example:
  {
    "color": "Red",
    "year": 2022
  }

Delete Automobile
  Method: DELETE
  URL: http://localhost:XXXX/api/automobiles/{vin}/

Service History
List Service History
  Method: GET
  URL: http://localhost:XXXX/api/service-history/

Filter Service History by VIN
  Method: GET
  URL: http://localhost:XXXX/api/service-history/?vin={vin}

Filter Service History by Technician
  Method: GET
  URL: http://localhost:XXXX/api/service-history/?technician={technician_id}

Filter Service History by Customer
  Method: GET
  URL: http://localhost:XXXX/api/service-history/?customer={customer_name}

List Technicians
Request:
  Method: GET
  URL: http://localhost:XXXX/api/technicians/
  Response:
  {
    "technicians": [
      {
        "first_name": "John",
        "last_name": "Doe",
        "technician_id": "JD123"
      },
      {
        "first_name": "Jane",
        "last_name": "Smith",
        "technician_id": "JS456"
      }
    ]
  }


## Sales Microservice
 **Setup**
   1. Fork, clone and build the project.
   2. Use these commands to build and and start docker containers:
      ```
      docker volume create beta-data
      docker-compose build
      docker-compose up
      ```


## Frontend
 Access the frontend of the project [here](http://localhost:3000/).

## Backend

**Use the links below to navigate to any portion of the backend of the project.**
   - Above each link is the HTTP request method.
   - Below each link to a service are the required JSON body values. If it's empty, no JSON body is required or an ID parameter is needed in the URL.

### Vehicle Models
1. **List vehicle models**
GET
[http://localhost:8100/api/models/](http://localhost:8100/api/models/)

2. **Create a vehicle model**
POST
[http://localhost:8100/api/models/](http://localhost:8100/api/models/)
    ```json
    {
      "name": "Chrysler",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer_id": 1
    }
    ```

3. **Get a specific vehicle model and its details**
GET
[http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)

4. **Update a vehicle model**
PUT
[http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)

5. **Delete a vehicle model**
DELETE
[http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)


### Automobiles
1. **List automobiles**
GET
[http://localhost:8100/api/automobiles/](http://localhost:8100/api/automobiles/)

2. **Create an automobile**
POST
[http://localhost:8100/api/automobiles/](http://localhost:8100/api/automobiles/)
    ```json
    {
      "color": "blue",
      "year": 2012,
      "vin": "1C3CC5FB2AN1201890",
      "model_id": 1
    }
    ```

3. **Get a specific automobile**
GET
[http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)

4. **Update an automobile**
PUT
[http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)
    ```json
    {
      "color": "red",
      "year": 2012,
      "sold": true
    }
    ```

5. **Delete an automobile**
DELETE
[http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)


### Manufacturers
1. **List manufacturers**
GET
[http://localhost:8100/api/manufacturers/](http://localhost:8100/api/manufacturers/)

2. **Create a manufacturer**
POST
[http://localhost:8100/api/manufacturers/](http://localhost:8100/api/manufacturers/)
    ```json
    {
      "name": "Chrysler"
    }
    ```

3. **Get a specific manufacturer**
GET
[http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)

4. **Update a manufacturer**
PUT
[http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)
    ```json
    {
      "name": "Chrysler"
    }
    ```

5. **Delete a manufacturer**
DELETE
[http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)


### Sales
1. **List sales**
GET
[http://localhost:8090/api/sales/](http://localhost:8090/api/sales/)

2. **Create a sale**
POST
[http://localhost:8090/api/sales/](http://localhost:8090/api/sales/)
    ```json
    {
        "price": "121.21",
        "salesperson": 11,
        "customer": 2,
        "automobile": 1
    }
    ```

3. **Delete a sale**
DELETE
[http://localhost:8090/api/sales/:id/](http://localhost:8090/api/sales/:id/)


### Customers
1. **List customers**
GET
[http://localhost:8090/api/customers/](http://localhost:8090/api/customers/)

2. **Create a customer**
POST
[http://localhost:8090/api/customers/](http://localhost:8090/api/customers/)
    ```json
    {
        "first_name":"test0",
        "last_name":"test1",
        "address":"test2",
        "phone_number": 123
    }
    ```

3. **Delete a specific customer**
DELETE
[http://localhost:8090/api/customers/:id/](http://localhost:8090/api/customers/:id/)


### Salespeople
1. **List salespeople**
GET
[http://localhost:8090/api/salespeople/](http://localhost:8090/api/salespeople/)

2. **Create a salesperson**
POST
[http://localhost:8090/api/salespeople/](http://localhost:8090/api/salespeople/)
    ```json
    {
        "employee_id":"SaleH",
        "first_name":"Sale",
        "last_name":"Sale"
    }
    ```

3. **Delete a specific salesperson**
DELETE
[http://localhost:8090/api/salespeople/:id/](http://localhost:8090/api/salespeople/:id/)
